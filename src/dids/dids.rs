use serde_json::{Value, Error};
use ring::digest::{Algorithm, Context, SHA512};
use merkle::{Hashable, MerkleTree, Proof};

#[allow(non_upper_case_globals)]
static digest: &'static Algorithm = &SHA512;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct PublicKey { 
    id: String,
    owner: String,
    publicKey : String
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Authentication {
    publicKey: String
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Service {
    serviceType: String,
    serviceEndpoint: String
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct DIDDoc {
    context: String,
    id : String,
    publicKey: PublicKey,
    authentication: Authentication,
    service:Service
}

pub fn create() {
        let values = (1..10).map(|x| vec![x]).collect::<Vec<_>>();

    let id = "did:ala:1233456".to_string();
    let key1 = "did:ala:1233456#key1".to_string();
    let public_key = PublicKey { id: key1.clone(), owner: id.clone(), publicKey:"2332432343442342".to_string()};
    let authentication = Authentication {publicKey: key1.clone()};
    let service  = Service {serviceType: "Information".to_string(), serviceEndpoint:"/ip4/127.0.0.1:3000/info".to_string()};

    let entity = DIDDoc {
        context : String::from("https://w3id.org/did/v1"),
        id : id,
        publicKey: public_key,
        authentication: authentication,
        service: service
     };

      let serialized = serde_json::to_string(&entity).unwrap();
    // println!("{:?}", entity);
    // println!("{:?}", serialized);
    // let tree = MerkleTree::from_vec(digest, values.clone());
    // println!("{:?}", tree);
} 